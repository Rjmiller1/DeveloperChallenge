
# Converts binary input from stdin to hex
def binStringToHex(data):
	result = hex(int(data,2))
	return result


# Converts hex to ascii string w/o using a library
def customHexToAscii(data):

	# Remove the hexadecimal 0x format
	data = data[2:].strip()

	# If string is notated as a long, it will 
	# remove the L at the tail of the string
	if(data[len(data)-1] == "L"):
		data = data[:-1]
	# Builds the ascii result
	result = ''.join([chr(int(''.join(c),16)) 
		for c in zip(data[0::2],data[1::2])])
	return result

# Combined process of bin->hex, hex->ascii functions
def binStringToAscii(data):
	hexVal = binStringToHex(data)
	asciiVal = customHexToAscii(hexVal)
	return asciiVal



