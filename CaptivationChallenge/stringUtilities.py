
def printNextHundered(data,keyword):

	# Check if given keyword is in data
	if((keyword in data) == False):
		return "Keyword:" + keyword + "not in string"

	# Set n to number of keywords in given string
	n = numSubstrings(data,keyword)
	
	index = indexOfLast(data,keyword)

 	resultString = data[index:index+100]
 	
 	return resultString

def numSubstrings(data,keyword):
	count = 0
	keyword_len = len(keyword)

	# Iterates through length of data
	for c in range(len(data)):
		# checks if current char is start of keyword and adds to count
		if data[c:c+keyword_len] == keyword:
			count += 1
	return count

def indexOfLast(data, keyword):

	# Number of keywords in data
	n = numSubstrings(data,keyword)

	# Position of first found keyword in data
	index = data.find(keyword)
	
	if(index == -1):
		return -1

	while index >= 0 and n > 1:
		# Adds to index for every keyword found in data
		index = data.find(keyword, index+1)
		n -= 1
	# Sets index to end of last occurance of keyword in data
	index = index + len(keyword)
	return index 


