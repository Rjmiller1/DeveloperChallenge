# Set the base image

FROM python:2.7

# Dockerfile author / maintainer

MAINTAINER Name rsmiller1@mix.wvu.edu

ADD Captivation.py bitConversion.py stringUtilities.py /

CMD [ "python", "./Captivation.py" ]

