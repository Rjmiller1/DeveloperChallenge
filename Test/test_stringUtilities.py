import unittest
import os
import sys

sys.path.insert(1,os.path.join(sys.path[0], '..'))

import CaptivationChallenge.stringUtilities

class testStringUtilities(unittest.TestCase):
	
	def testNumStrings(self):

		data = "helloWorldhelloWorldhellohello"
		sub = "hello"
		self.assertAlmostEqual(CaptivationChallenge.stringUtilities.numSubstrings(data,sub),4)

		data = "helloHelloWorld"
		self.assertAlmostEqual(CaptivationChallenge.stringUtilities.numSubstrings(data,sub),1)

		data = "helloWorldhelloWorldhellohello"
		sub = "grep"
		self.assertAlmostEqual(CaptivationChallenge.stringUtilities.numSubstrings(data,sub),0)

	def testIndexOfLast(self):

		data_1 = "helloWorldhelloWorldhellohello"
		sub_1 = "hello"
		index = len(data_1)
		self.assertAlmostEqual(CaptivationChallenge.stringUtilities.indexOfLast(data_1,sub_1),index)

		data_2 = "capcapWorldcapHellocapcapfoo"
		sub_2 = "cap"
		self.assertAlmostEqual(CaptivationChallenge.stringUtilities.indexOfLast(data_2,sub_2),25)

		data_3 = "weAreTheChampions"
		sub_3 = "weare"
		self.assertAlmostEqual(CaptivationChallenge.stringUtilities.indexOfLast(data_3,sub_3),-1)

	def testPrintNextHundred(self):
		with open('TestData.txt', 'r') as myfile:
 			text = myfile.read()

		self.assertAlmostEqual(len(CaptivationChallenge.stringUtilities.printNextHundered(text,"CAPTIVATION")),100)

if __name__ == '__main__':
    unittest.main()