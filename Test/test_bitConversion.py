import unittest
import os
import sys

sys.path.insert(1,os.path.join(sys.path[0], '..'))

import CaptivationChallenge.bitConversion

class testBitConversion(unittest.TestCase):
	
	def testBinStringToHex(self):

		data1 = "01101100"
		self.assertAlmostEqual(CaptivationChallenge.bitConversion.binStringToHex(data1),"0x6c")

		data2 = "0010101"
		self.assertAlmostEqual(CaptivationChallenge.bitConversion.binStringToHex(data2),"0x15")

	def testCustomHexToAscii(self):

		data1 = "0x41"
		self.assertAlmostEqual(CaptivationChallenge.bitConversion.customHexToAscii(data1),"A")

		data2 = "0x414243"
		self.assertAlmostEqual(CaptivationChallenge.bitConversion.customHexToAscii(data2),"ABC")

if __name__ == '__main__':
    unittest.main()