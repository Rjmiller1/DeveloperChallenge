Here is my submission for the Developer Challenge.


To run/build the project via Docker: 
```
docker pull rjmiller1/captivationchallenge
```

```
echo 0100000101000010 | docker run -i a13fe4c41878
```
or 
```
cat file.txt | docker run -i a13fe4c41878
```

If there are issues with the building through Docker simply clone the project,
echo your input and run Captivation.py (located in CaptivationChellenge directory) 
as shown below or cat if input is coming 
from a file as followed below.

```
echo 0100000101000010 | python Captivation.py
```

```
cat file.txt | python Captivation.py
```
